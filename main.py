from base64 import b64decode
from time import monotonic
from typing import List, Tuple

import cv2
import filetype
import numpy as np
from fastapi import FastAPI, Response
from matplotlib.colors import hex2color
from pydantic import BaseModel, validator
from skimage.draw import polygon


class Properties(BaseModel):
    intensity: float


class Geometry(BaseModel):
    coordinates: list


class Feature(BaseModel):
    type: str
    geometry: Geometry
    properties: Properties


class VectorMask(BaseModel):
    fill: str
    name: str
    type: str
    features: List[Feature]
    view_box: str
    mask_type: str
    tech_name: str


class Request(BaseModel):
    image_b64: bytes
    vector_mask: VectorMask

    @validator("image_b64")
    def validate_b64(cls, value: bytes) -> bytes:
        return b64decode(value)

    @property
    def mime(self) -> str:
        return filetype.guess_mime(self.image_b64)

    @property
    def extension(self) -> str:
        return filetype.guess_extension(self.image_b64)


app = FastAPI(docs_url="/concealer/docs", openapi_url="/concealer/openapi.json")


def view_box_to_shape(view_box: str) -> Tuple[int, int]:
    """Supplementary method to convert viewBox to image shape.

    :param view_box: str, view_box
    :return: tuple, image shape, (height, width)
    """
    view_box = [int(elem) for elem in view_box.split(" ")]
    return view_box[3] - view_box[1], view_box[2] - view_box[0]


def draw_multipolygons(vector_mask: VectorMask):
    """Function to rasterize vector polygon mask

    :return: np.ndarray, np.float32 2D mask with values in range [0, 1]
    """
    img_shape = view_box_to_shape(vector_mask.view_box)
    canvas = np.zeros(img_shape, dtype=np.uint8)
    for feature in vector_mask.features:
        intensity = (
            np.uint8(feature.properties.intensity * 255)
            if feature.properties.intensity
            else 255
        )
        for polygon_object in feature.geometry.coordinates:
            external_contours = np.array(polygon_object[0])
            rr, cc = polygon(
                external_contours[:, 1], external_contours[:, 0], canvas.shape
            )
            canvas[rr, cc] += intensity
            if len(polygon_object) > 1:
                for contour in polygon_object[1:]:
                    internal_contour = np.array(contour)
                    rr, cc = polygon(
                        internal_contour[:, 1], internal_contour[:, 0], canvas.shape
                    )
                    canvas[rr, cc] -= intensity
        canvas = np.clip(canvas, 0, 255)
    return canvas


def draw_multipoints(vector_mask: VectorMask) -> np.ndarray:
    """Function to rasterize vector points mask

    :param vector_mask: dict describing a mask
    :return: np.ndarray, np.float32 2D mask with values in range [0, 1]
    """
    img_shape = view_box_to_shape(vector_mask.view_box)
    canvas = np.zeros(img_shape, dtype=np.uint8)
    for feature in vector_mask.features:
        intensity = (
            int(feature.properties.intensity * 255)
            if feature.properties.intensity
            else 255
        )
        for point_object in feature.geometry.coordinates:
            canvas = cv2.circle(
                canvas,
                tuple(point_object[:2]),
                point_object[2],
                color=intensity,
                thickness=-1,
            )
    return canvas.astype(np.uint8)


def color_hex2rgb(hex_color: str) -> tuple:
    """Function to convert color from HEX to RGB"""
    return tuple([int(255 * value) for value in hex2color(hex_color)])


def overlay_mask(
    image: np.ndarray, vector_mask: VectorMask, fill=None, opacity=0.8
) -> np.ndarray:

    """Method to overlay vector mask on given image

    :param image: np.ndarray, RGB / RGB+A image, np.uint8
    :param vector_mask: dict containing vector masks
    :param fill: fill color in HEX format.
        Example: '#ffffff' for white color. If left `None`,
        default color #2f63d9 is used. Default is `None`
    :param opacity: opacity of mask
    :return: overlaid_image, np.ndarray, RGB / RGB+A image, np.uint8
    """

    image_rgb = image[:, :, :3]

    if vector_mask.mask_type in ["polygon_mask", "heatmap_mask"]:
        mask = draw_multipolygons(vector_mask)
    else:
        mask = draw_multipoints(vector_mask)

    if fill is None:
        if vector_mask.fill is None:
            fill = "#2f63d9"
        else:
            fill = vector_mask.fill

    rgb_color = color_hex2rgb(fill)

    canvas = image.copy().astype(np.float32)
    for channel in range(len(rgb_color)):
        channel_intensity = rgb_color[channel]
        canvas[:, :, channel] -= canvas[:, :, channel] * opacity * mask
        canvas[:, :, channel] += mask * opacity * channel_intensity
    return canvas.clip(0, 255).astype(np.uint8)


def load_image_from_bytes(image_bytes: bytes, *, bgr2rgb_inversion=True) -> np.ndarray:
    image = cv2.imdecode(np.frombuffer(image_bytes, np.uint8), -1)
    if image.shape[2] == 4:
        convert = cv2.COLOR_BGRA2RGBA
    else:
        convert = cv2.COLOR_BGR2RGB
    if bgr2rgb_inversion:
        image = cv2.cvtColor(image, convert)
    return image


def dump_image_to_bytes(image: np.ndarray, extension=".jpg") -> bytes:
    if image.shape[2] == 4:
        if extension != ".png":
            raise ValueError(
                f"Image has 4 channels, it's extension"
                f" must be '.png', but received {extension}"
            )
        convert = cv2.COLOR_RGBA2BGRA
    else:
        convert = cv2.COLOR_RGB2BGR
    return cv2.imencode(extension, cv2.cvtColor(image, convert))[1].tobytes()


@app.post("/concealer/mask/")
def mask(request: Request):
    start = monotonic()
    img = load_image_from_bytes(request.image_b64)
    masked_image = overlay_mask(img, request.vector_mask)
    image_bytes = dump_image_to_bytes(image=masked_image)
    return Response(
        image_bytes,
        media_type=request.mime,
        headers={"x-elapsed-time-sec": f"{monotonic() - start:.4f}"},
    )
