import json
from base64 import b64encode

import requests
from fastapi.testclient import TestClient

from main import app

example_result = {
    "image_type": "selfie",
    "area_results": [
        {
            "area_name": "face",
            "main_metric": {
                "name": "Wrinkles Score",
                "value": 99,
                "tech_name": "wrinkles_score",
                "widget_meta": None,
                "widget_type": "bad_good_line",
            },
            "sub_metrics": None,
        },
        {
            "area_name": "forehead",
            "main_metric": {
                "name": "Wrinkles Score",
                "value": 99,
                "tech_name": "wrinkles_score",
                "widget_meta": None,
                "widget_type": "bad_good_line",
            },
            "sub_metrics": [
                {
                    "name": "Deep Wrinkles Density",
                    "value": 0,
                    "tech_name": "deep_wrinkles_density",
                    "widget_meta": None,
                    "widget_type": "density",
                },
                {
                    "name": "Fine Wrinkles Density",
                    "value": 0,
                    "tech_name": "fine_wrinkles_density",
                    "widget_meta": None,
                    "widget_type": "density",
                },
            ],
        },
        {
            "area_name": "right_eye_area",
            "main_metric": {
                "name": "Wrinkles Score",
                "value": 99,
                "tech_name": "wrinkles_score",
                "widget_meta": None,
                "widget_type": "bad_good_line",
            },
            "sub_metrics": [
                {
                    "name": "Deep Wrinkles Density",
                    "value": 0,
                    "tech_name": "deep_wrinkles_density",
                    "widget_meta": None,
                    "widget_type": "density",
                },
                {
                    "name": "Fine Wrinkles Density",
                    "value": 0,
                    "tech_name": "fine_wrinkles_density",
                    "widget_meta": None,
                    "widget_type": "density",
                },
            ],
        },
        {
            "area_name": "left_eye_area",
            "main_metric": {
                "name": "Wrinkles Score",
                "value": 98,
                "tech_name": "wrinkles_score",
                "widget_meta": None,
                "widget_type": "bad_good_line",
            },
            "sub_metrics": [
                {
                    "name": "Deep Wrinkles Density",
                    "value": 1,
                    "tech_name": "deep_wrinkles_density",
                    "widget_meta": None,
                    "widget_type": "density",
                },
                {
                    "name": "Fine Wrinkles Density",
                    "value": 0,
                    "tech_name": "fine_wrinkles_density",
                    "widget_meta": None,
                    "widget_type": "density",
                },
            ],
        },
    ],
    "masks_original": [
        {
            "fill": "#ffffff",
            "name": "Wrinkles Mask",
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "feature",
                    "geometry": {
                        "type": "MultiPolygon",
                        "coordinates": [
                            [
                                [
                                    [554, 913],
                                    [554, 916],
                                    [555, 917],
                                    [555, 921],
                                    [556, 923],
                                    [558, 925],
                                    [560, 926],
                                    [562, 927],
                                    [565, 927],
                                    [567, 929],
                                    [569, 929],
                                    [571, 931],
                                    [571, 932],
                                    [573, 934],
                                    [574, 934],
                                    [576, 938],
                                    [577, 944],
                                    [580, 937],
                                    [579, 933],
                                    [580, 930],
                                    [573, 930],
                                    [571, 929],
                                    [567, 925],
                                    [563, 922],
                                    [561, 922],
                                    [559, 920],
                                    [556, 913],
                                    [556, 911],
                                ]
                            ],
                            [
                                [
                                    [615, 904],
                                    [618, 905],
                                    [620, 907],
                                    [627, 910],
                                    [634, 912],
                                    [636, 912],
                                    [637, 911],
                                    [640, 911],
                                    [645, 913],
                                    [647, 915],
                                    [649, 915],
                                    [651, 914],
                                    [651, 911],
                                    [646, 911],
                                    [640, 909],
                                    [631, 909],
                                    [624, 904],
                                ]
                            ],
                            [
                                [
                                    [225, 858],
                                    [223, 859],
                                    [222, 862],
                                    [223, 863],
                                    [219, 870],
                                    [218, 873],
                                    [218, 876],
                                    [221, 874],
                                    [221, 873],
                                    [225, 869],
                                    [227, 865],
                                ]
                            ],
                            [
                                [
                                    [213, 857],
                                    [211, 858],
                                    [207, 862],
                                    [206, 864],
                                    [205, 867],
                                    [205, 870],
                                    [207, 870],
                                    [210, 867],
                                    [211, 864],
                                    [216, 860],
                                    [218, 860],
                                    [220, 859],
                                    [220, 857],
                                    [218, 856],
                                ]
                            ],
                        ],
                    },
                    "properties": {"intensity": 1.0},
                }
            ],
            "view_box": "0 0 1080 1920",
            "mask_type": "polygon_mask",
            "tech_name": "wrinkles_polygon_mask_original",
        }
    ],
    "masks_restored": [
        {
            "fill": "#ffffff",
            "name": "Wrinkles Mask",
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "feature",
                    "geometry": {
                        "type": "MultiPolygon",
                        "coordinates": [
                            [
                                [
                                    [690, 597],
                                    [690, 600],
                                    [691, 601],
                                    [691, 605],
                                    [692, 607],
                                    [694, 609],
                                    [696, 610],
                                    [698, 611],
                                    [701, 611],
                                    [703, 613],
                                    [705, 613],
                                    [707, 615],
                                    [707, 616],
                                    [709, 618],
                                    [710, 618],
                                    [712, 622],
                                    [713, 628],
                                    [716, 621],
                                    [715, 617],
                                    [716, 614],
                                    [709, 614],
                                    [707, 613],
                                    [703, 609],
                                    [699, 606],
                                    [697, 606],
                                    [695, 604],
                                    [692, 597],
                                    [692, 595],
                                ]
                            ],
                            [
                                [
                                    [751, 587],
                                    [754, 588],
                                    [756, 590],
                                    [763, 593],
                                    [770, 595],
                                    [772, 595],
                                    [773, 594],
                                    [776, 594],
                                    [781, 596],
                                    [783, 598],
                                    [785, 598],
                                    [787, 597],
                                    [787, 594],
                                    [782, 594],
                                    [776, 592],
                                    [767, 592],
                                    [760, 587],
                                ]
                            ],
                            [
                                [
                                    [360, 548],
                                    [358, 549],
                                    [357, 552],
                                    [358, 553],
                                    [354, 560],
                                    [353, 563],
                                    [353, 566],
                                    [356, 564],
                                    [356, 563],
                                    [360, 559],
                                    [362, 555],
                                ]
                            ],
                            [
                                [
                                    [348, 547],
                                    [346, 548],
                                    [342, 552],
                                    [341, 554],
                                    [340, 557],
                                    [340, 560],
                                    [342, 560],
                                    [345, 557],
                                    [346, 554],
                                    [351, 550],
                                    [353, 550],
                                    [355, 549],
                                    [355, 547],
                                    [353, 546],
                                ]
                            ],
                        ],
                    },
                    "properties": {"intensity": 1.0},
                }
            ],
            "view_box": "0 0 1214 1214",
            "mask_type": "polygon_mask",
            "tech_name": "wrinkles_polygon_mask_restored",
        }
    ],
    "algorithm_tech_name": "wrinkles",
}


def test():
    client = TestClient(app)

    with open("test.jpg", "rb") as image_file:
        image_bytes = image_file.read()
        image_bytes = b64encode(image_bytes)

    for mask in example_result["masks_original"]:
        resp = client.post(
            "/concealer/mask/",
            json={
                "vector_mask": mask,
                "image_b64": image_bytes.decode(),
            },
        )
        print(resp.headers)
        assert resp.status_code == 200
        assert len(resp.text) != 0
